`include "delays.v"

module io(rst_n, data, addr, mem_clk_n, rm_n, switches, leds);
    input rst_n, mem_clk_n, rm_n;
    inout [7:0] data;
    input [7:0] addr;
    input [15:0] switches;
    output [15:0] leds;

    wire not_addr7, not_addr6, ad_int0, ad_int1, ad_dec_en_n;
    wire [3:0] addr_sel_n;
    `NOT_PD not_addr7_gate(not_addr7, addr[7]);
    `NOT_PD not_addr6_gate(not_addr6, addr[6]);
    `OR_PD ad_int0_gate(ad_int0, not_addr7, not_addr6, addr[5]);
    `OR_PD ad_int1_gate(ad_int1, addr[4], addr[3], addr[2]);
    `OR_PD ad_dec_en_n_date(ad_dec_en_n, ad_int0, ad_int1, 1'b0);
    decoder_2x4 io_addr_en_dec(addr_sel_n, addr[1:0], ad_dec_en_n);

    wire out0_clk, out1_clk;
    `NOR_PD out0_clk_gate(out0_clk, mem_clk_n, addr_sel_n[0]);
    register_nbit out0_reg(out0_clk, ~rst_n, 1'b1, data, leds[15:8]);
    defparam out0_reg.N = 8;
    `NOR_PD out1_clk_gate(out1_clk, mem_clk_n, addr_sel_n[1]);
    register_nbit out1_reg(out1_clk, ~rst_n, 1'b1, data, leds[7:0]);
    defparam out1_reg.N = 8;

    wire in0_en_n, in1_en_n;
    `OR_PD in0_en_n_gate(in0_en_n, rm_n, addr_sel_n[2]);
    //assign data = in0_en_n ? 8'bz : switches[15:8];
    tri_state_buf sw_en_inst0(in0_en_n, switches[15:8], data);
    `OR_PD in1_en_n_gate(in1_en_n, rm_n, addr_sel_n[3]);
    //assign data = in1_en_n ? 8'bz : switches[7:0];
    tri_state_buf sw_en_inst1(in1_en_n, switches[7:0], data);

endmodule

