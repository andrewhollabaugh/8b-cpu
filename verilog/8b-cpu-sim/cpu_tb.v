module cpu_tb();
    reg clk, rst_n;
    reg [15:0] switches;
    wire [2:0] comp;
    wire [7:0] data, addr, K, PC, A, B;
    wire [13:0] control_word;
    wire [15:0] I, leds;

    wire ir_clk, pc_clk, a_clk, b_clk, addr_clk, mem_clk_n, pcl_n, ralu_n, rk_n, rm_n, ci, binv;
    wire [1:0] FS;

    wire nop;

    assign ir_clk = control_word[13];
    assign pc_clk = control_word[12];
    assign a_clk = control_word[11];
    assign b_clk = control_word[10];
    assign addr_clk = control_word[9];
    assign mem_clk_n = control_word[8];
    assign pcl_n = control_word[7];
    assign ralu_n = control_word[6];
    assign rk_n = control_word[5];
    assign rm_n = control_word[4];
    assign ci = control_word[3];
    assign binv = control_word[2];
    assign FS = control_word[1:0];

    cpu cpu_inst(clk, rst_n, comp, data, addr, control_word, K, I, PC, A, B, switches, leds, nop);

    always #200 clk <= ~clk;

    initial begin
        clk <= 1'b0;
        rst_n <= 1'b0;
        switches <= 16'b0;
        #1250
        rst_n <= 1'b1;
        switches <= 16'h4006;
        #100000
        $stop;
    end

endmodule

