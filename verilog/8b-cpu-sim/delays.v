`ifndef delays_v
`define delays_v

`define ram_pd          55
`define rom_pd          70
`define mux4to1_pd      40
`define decoder_2x4_pd  30
`define reg_pd          30
`define counter_pd      80
`define adder_pd        40
`define comparator_pd   40
`define buf_pd          20

`define NAND_PD nand    #25
`define NOR_PD nor      #25
`define AND_PD and      #25
`define OR_PD or        #25
`define XOR_PD xor       #25
`define NOT_PD not      #10

`endif

