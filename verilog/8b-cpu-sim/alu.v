`include "delays.v"

module alu(A, B, FS, ci, binv, F, comp);
	input [7:0] A, B;
	input [1:0] FS;
	// FS - function select
	//   00 - AND
	//   01 - OR
	//   10 - XOR
	//   11 - ADD/SUB
	input ci, binv;
    output [2:0] comp;
	output [7:0] F;
	
	wire [7:0] B_after_inv;
    //assign B_after_inv = B ^ {8{binv}};
	genvar i_xor1;
	generate
	for (i_xor1 = 0; i_xor1 < 8; i_xor1 = i_xor1 + 1) begin: binv_xors
        `XOR_PD binv_xor_inst(B_after_inv[i_xor1], B[i_xor1], binv);
	end
	endgenerate
	
	wire [7:0] and_output, or_output, xor_output, add_output, shift_left, shift_right;
    
    //assign and_output = A & B_after_inv;
	genvar i_and;
	generate
	for (i_and = 0; i_and < 8; i_and = i_and + 1) begin: alu_ands
        `AND_PD alu_and_inst(and_output[i_and], A[i_and], B_after_inv[i_and]);
	end
	endgenerate

    //assign or_output = A | B_after_inv;
	genvar i_or;
	generate
	for (i_or = 0; i_or < 8; i_or = i_or + 1) begin: alu_ors
        `OR_PD alu_or_inst(or_output[i_or], A[i_or], B_after_inv[i_or]);
	end
	endgenerate

    //assign xor_output = A ^ B_after_inv;
	genvar i_xor2;
	generate
	for (i_xor2 = 0; i_xor2 < 8; i_xor2 = i_xor2 + 1) begin: alu_xors
        `XOR_PD alu_xor_inst(xor_output[i_xor2], A[i_xor2], B_after_inv[i_xor2]);
	end
	endgenerate

    wire co;
	adder adder(A, B_after_inv, ci, add_output, co);
	
	mux4to1nbit main_mux(F, FS, and_output, or_output, xor_output, add_output);
    defparam main_mux.N = 8;

    comparator comp_inst(A, B, comp[0], comp[1], comp[2]);

endmodule

module adder(A, B, ci, S, co);
	input [7:0] A, B;
	input ci;
	output [7:0] S;
	output co;
	
	wire [8:0] carry;
	assign carry[0] = ci;
	
	// use generate block to instantiate 8 full adders
	genvar i;
	generate
	for (i=0; i<8; i=i+1) begin: full_adders // blocks within a generate block need to be named
		full_adder adder_inst(A[i], B[i], carry[i], S[i], carry[i+1]);	
	end
	endgenerate
	
	assign co = carry[8];
endmodule

module full_adder(a, b, ci, s, co);
	input a, b, ci;
	output reg s, co;
	
    always @* begin
	    #(`adder_pd) s <= a ^ b ^ ci;
	    co <= a&b | a&ci | b&ci;
    end
endmodule

