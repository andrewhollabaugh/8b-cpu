module cpu(clk, rst_n, comp, data, addr, control_word, K, I, PC, A, B, switches, leds, nop);
    input clk, rst_n;
    input [15:0] switches;
    output [2:0] comp;
    output [7:0] data, addr, K, PC, A, B;
    output [13:0] control_word;
    output [15:0] I, leds;
    
    output nop;

    id id_inst(clk, I, comp, control_word, nop);
    datapath datapath_inst(rst_n, control_word, I, PC, comp, data, addr, K, A, B, switches, leds);

endmodule

