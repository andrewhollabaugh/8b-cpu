`include "delays.v"

module tri_state_buf(oe_n, in, out);
    input oe_n;
    input [7:0] in;
    output reg [7:0] out;

    always @* begin
        #(`buf_pd)
        if(!oe_n)
            out <= in;
        else
            out <= 8'bz;
    end

endmodule

module ram(data, addr, cs_n, we_n, re_n);
    input cs_n, we_n, re_n;
    input [7:0] addr;
    inout [7:0] data;
    reg [7:0] mem [0:(1 << 8) - 1];
    reg [7:0] data_out;

    assign data = (!cs_n && we_n && !re_n) ? data_out : 8'bz;

    always@* begin
        #(`ram_pd)
        if(!cs_n && !we_n) mem[addr] <= data;
        data_out <= mem[addr];
    end
endmodule

module mux4to1nbit(F, S, I0, I1, I2, I3);
    parameter N = 8;
    input [N-1:0]I0, I1, I2, I3;
    input [1:0]S;
    output reg [N-1:0]F;

    always @* #(`mux4to1_pd) F <= S[1] ? (S[0] ? I3 : I2) : (S[0] ? I1 : I0);
endmodule

module decoder_2x4(m, s, en_n);
    input [1:0] s; // select
    input en_n; // enable (negative logic)
    output reg [3:0] m;

    always @(s or en_n) begin
        #(`decoder_2x4_pd)
        if(!en_n) begin
            case(s)
                2'b00: m <= 4'b1110;
                2'b01: m <= 4'b1101;
                2'b10: m <= 4'b1011;
                2'b11: m <= 4'b0111;
                default: m <= 4'b1111;
            endcase
        end
        else m <= 4'b1111;
    end
endmodule

module register_nbit(clk, rst, l, D, Q);
    parameter N = 8; // number of bits
    input clk; // positive edge clock
    input rst; // positive logic asynchronous reset
    input l; // load enable
    input [N-1:0] D; // data input
    output reg [N-1:0] Q; // registered output

    always @(posedge clk or posedge rst) begin
        #(`reg_pd)
        if(rst)
            Q <= 0;
        else if(l)
            Q <= D;
        else
            Q <= Q;
    end
endmodule

module counter_8bit(clk, rst, l, D, Q);
    input clk; // positive edge clock
    input rst; // positive logic asynchronous reset
    input l; // load enable
    input [7:0] D; // data input
    output reg [7:0] Q; // registered output

    always @(posedge clk or posedge rst) begin
        #(`counter_pd)
        if(rst)
            Q <= 0;
        else if(l)
            Q <= D;
        else if(Q == 8'hFF)
            Q <= 0;
        else
            Q <= Q + 1'b1;
    end
endmodule

module comparator(A, B, equals, less_than, greater_than);
    input [7:0] A, B;
    output reg equals, less_than, greater_than;

    always @* begin
        #(`comparator_pd)
        if(A == B) begin
            equals <= 1'b1;
            less_than <= 1'b0;
            greater_than <= 1'b0;
        end
        else if(A < B) begin
            equals <= 1'b0;
            less_than <= 1'b1;
            greater_than <= 1'b0;
        end
        else begin
            equals <= 1'b0;
            less_than <= 1'b0;
            greater_than <= 1'b1;
        end
    end

endmodule

