`include "delays.v"

module id(clk, I, comp, control_word, nop);
    input clk;
    input [2:0] comp;
    input [15:0] I;
    output [13:0] control_word;

    output nop;

    wire ir_clk, pc_clk, a_clk, b_clk, addr_clk, mem_clk_n, pcl_n, ralu_n, rk_n, rm_n, ci, binv;
    wire [1:0] FS;

    wire not_I15, not_I14, not_I13;
    `NOT_PD not_I15_gate(not_I15, I[15]);
    `NOT_PD not_I14_gate(not_I14, I[14]);
    `NOT_PD not_I13_gate(not_I13, I[13]);

    assign ir_clk = clk;
    `NOT_PD not_pc(pc_clk, clk);

    wire nop_int0, nop_int1;
    `NOR_PD nop_nor0(nop_int0, I[15], I[14], I[13]);
    `NOR_PD nop_nor1(nop_int1, not_I15, not_I14, not_I13);
    `OR_PD nop_or(nop, nop_int0, nop_int1);

    wire wen_n, mem_clk;
    wire [3:0] wens;
    `OR_PD wen_or(wen_n, I[14], nop);
    decoder_2x4 wen_dec(wens, I[1:0], wen_n);
    `NOR_PD a_clk_nor(a_clk, wens[0], clk);
    `NOR_PD b_clk_nor(b_clk, wens[1], clk);
    `NOR_PD addr_clk_nor(addr_clk, wens[2], clk);
    `NOR_PD mem_clk_nor(mem_clk, wens[3], clk);
    `NOT_PD mem_clk_not(mem_clk_n, mem_clk);

    wire pcl_int0, pcl_int1, pcl_int2, not_equals;
    `NOT_PD not_equals_gate(not_equals, comp[0]);
    mux4to1nbit pcl_mux(pcl_int0, I[11:10], comp[0], not_equals, comp[1], comp[2]);
    defparam pcl_mux.N = 1;
    `NOR_PD pcl_nor(pcl_int1, 1'b0, pcl_int0, I[12]);
    `OR_PD pcl_or1(pcl_int2, I[13], nop);
    `OR_PD pcl_or2(pcl_n, pcl_int1, pcl_int2);

    `NAND_PD rm_nand(rm_n, I[13], I[12]);
    `NAND_PD rk_nand(rk_n, I[15], not_I14);
    `NAND_PD ralu_nand(ralu_n, rm_n, rk_n);

    assign ci = I[9];
    assign binv = I[9];
    assign FS[1] = I[11];
    assign FS[0] = I[10];

    assign control_word = {ir_clk, pc_clk, a_clk, b_clk, addr_clk, mem_clk_n, pcl_n, ralu_n, rk_n, rm_n, ci, binv, FS};

endmodule

