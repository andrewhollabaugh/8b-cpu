`include "delays.v"

module datapath(rst_n, control_word, I, PC, comp, data, addr, K, A, B, switches, leds);
	input rst_n;
	input [13:0] control_word;
    input [15:0] switches;
    output [2:0] comp;
	output [7:0] data, addr, K, PC, A, B;
    output [15:0] I, leds;
	
    wire ir_clk, pc_clk, a_clk, b_clk, addr_clk, mem_clk_n, pcl_n, ralu_n, rk_n, rm_n, ci, binv;
	wire [1:0] FS;

    assign ir_clk = control_word[13];
    assign pc_clk = control_word[12];
    assign a_clk = control_word[11];
    assign b_clk = control_word[10];
    assign addr_clk = control_word[9];
    assign mem_clk_n = control_word[8];
    assign pcl_n = control_word[7];
    assign ralu_n = control_word[6];
    assign rk_n = control_word[5];
    assign rm_n = control_word[4];
    assign ci = control_word[3];
    assign binv = control_word[2];
    assign FS = control_word[1:0];

    wire [15:0] rom_data;
    rom rom_inst(rom_data, PC);
    register_nbit ir_inst(ir_clk, ~rst_n, 1'b1, rom_data, I);
    defparam ir_inst.N = 16;
    counter_8bit pc_inst(pc_clk, ~rst_n, ~pcl_n, K, PC);
    assign K = I[9:2];

    wire [7:0] A, B, F;
    register_nbit a_reg_inst(a_clk, ~rst_n, 1'b1, data, A);
    defparam a_reg_inst.N = 8;
    register_nbit b_reg_inst(b_clk, ~rst_n, 1'b1, data, B);
    defparam b_reg_inst.N = 8;
    register_nbit addr_reg_inst(addr_clk, ~rst_n, 1'b1, data, addr);
    defparam addr_reg_inst.N = 8;
    alu alu_inst(A, B, FS, ci, binv, F, comp);
    assign data = ralu_n ? 8'bz : F;
    tri_state_buf rk_buf_inst(rk_n, K, data);

    wire ram_cs_n;
    and ram_addr_detect_and_gate(ram_cs_n, addr[7], addr[6]);
    ram ram_inst(data, addr, ram_cs_n, mem_clk_n, rm_n);

    io io_inst(rst_n, data, addr, mem_clk_n, rm_n, switches, leds);
    
endmodule

