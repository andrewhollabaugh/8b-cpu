EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Timer:TLC555xD U?
U 1 1 5FB17A33
P 4950 3550
AR Path="/5FB17A33" Ref="U?"  Part="1" 
AR Path="/5FAFFE1A/5FB17A33" Ref="U19"  Part="1" 
F 0 "U19" H 4650 3900 50  0000 C CNN
F 1 "TLC555xD" H 4950 3550 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5800 3150 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/texas-instruments/TLC555CDR/276979" H 5800 3150 50  0001 C CNN
	1    4950 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5FB17A3F
P 5100 3050
AR Path="/5FB17A3F" Ref="C?"  Part="1" 
AR Path="/5FAFFE1A/5FB17A3F" Ref="C9"  Part="1" 
F 0 "C9" V 5000 2950 50  0000 L CNN
F 1 "100n" V 5050 3100 50  0000 L CNN
F 2 "" H 5100 3050 50  0001 C CNN
F 3 "~" H 5100 3050 50  0001 C CNN
	1    5100 3050
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 5FB17A45
P 5700 3700
AR Path="/5FB17A45" Ref="R?"  Part="1" 
AR Path="/5FAFFE1A/5FB17A45" Ref="R6"  Part="1" 
F 0 "R6" H 5550 3650 50  0000 L CNN
F 1 "200" H 5500 3750 50  0000 L CNN
F 2 "" H 5700 3700 50  0001 C CNN
F 3 "~" H 5700 3700 50  0001 C CNN
	1    5700 3700
	-1   0    0    1   
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 5FB17A4B
P 5700 3400
AR Path="/5FB17A4B" Ref="R?"  Part="1" 
AR Path="/5FAFFE1A/5FB17A4B" Ref="R5"  Part="1" 
F 0 "R5" H 5550 3350 50  0000 L CNN
F 1 "500" H 5500 3450 50  0000 L CNN
F 2 "" H 5700 3400 50  0001 C CNN
F 3 "~" H 5700 3400 50  0001 C CNN
	1    5700 3400
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5FB17A51
P 5700 4350
AR Path="/5FB17A51" Ref="C?"  Part="1" 
AR Path="/5FAFFE1A/5FB17A51" Ref="C12"  Part="1" 
F 0 "C12" H 5750 4450 50  0000 L CNN
F 1 "1u" H 5800 4350 50  0000 L CNN
F 2 "" H 5700 4350 50  0001 C CNN
F 3 "~" H 5700 4350 50  0001 C CNN
	1    5700 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5FB17A57
P 4250 2300
AR Path="/5FB17A57" Ref="C?"  Part="1" 
AR Path="/5FAFFE1A/5FB17A57" Ref="C8"  Part="1" 
F 0 "C8" V 4150 2150 50  0000 L CNN
F 1 "100n" V 4150 2300 50  0000 L CNN
F 2 "" H 4250 2300 50  0001 C CNN
F 3 "~" H 4250 2300 50  0001 C CNN
	1    4250 2300
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW?
U 1 1 5FB17A5D
P 4400 2300
AR Path="/5FB17A5D" Ref="SW?"  Part="1" 
AR Path="/5FAFFE1A/5FB17A5D" Ref="SW2"  Part="1" 
F 0 "SW2" H 4150 2400 50  0000 L CNN
F 1 "CLK Button" H 4150 2500 50  0000 L CNN
F 2 "" H 4400 2500 50  0001 C CNN
F 3 "~" H 4400 2500 50  0001 C CNN
	1    4400 2300
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 5FB17A63
P 4400 2650
AR Path="/5FB17A63" Ref="R?"  Part="1" 
AR Path="/5FAFFE1A/5FB17A63" Ref="R3"  Part="1" 
F 0 "R3" H 4250 2700 50  0000 L CNN
F 1 "1k" H 4250 2600 50  0000 L CNN
F 2 "" H 4400 2650 50  0001 C CNN
F 3 "~" H 4400 2650 50  0001 C CNN
	1    4400 2650
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5FB17A69
P 4950 3050
AR Path="/5FB17A69" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17A69" Ref="#PWR017"  Part="1" 
F 0 "#PWR017" H 4950 2900 50  0001 C CNN
F 1 "+3V3" H 4965 3223 50  0000 C CNN
F 2 "" H 4950 3050 50  0001 C CNN
F 3 "" H 4950 3050 50  0001 C CNN
	1    4950 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR?
U 1 1 5FB17A6F
P 5200 3050
AR Path="/5FB17A6F" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17A6F" Ref="#PWR021"  Part="1" 
F 0 "#PWR021" H 5200 2800 50  0001 C CNN
F 1 "GNDREF" H 5205 2877 50  0001 C CNN
F 2 "" H 5200 3050 50  0001 C CNN
F 3 "" H 5200 3050 50  0001 C CNN
	1    5200 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 3050 4950 3050
Wire Wire Line
	4950 3050 4950 3150
Connection ~ 4950 3050
$Comp
L power:GNDREF #PWR?
U 1 1 5FB17A78
P 4950 3950
AR Path="/5FB17A78" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17A78" Ref="#PWR018"  Part="1" 
F 0 "#PWR018" H 4950 3700 50  0001 C CNN
F 1 "GNDREF" H 4955 3777 50  0001 C CNN
F 2 "" H 4950 3950 50  0001 C CNN
F 3 "" H 4950 3950 50  0001 C CNN
	1    4950 3950
	1    0    0    -1  
$EndComp
NoConn ~ 4450 3550
$Comp
L power:+3V3 #PWR?
U 1 1 5FB17A7F
P 5700 3300
AR Path="/5FB17A7F" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17A7F" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 5700 3150 50  0001 C CNN
F 1 "+3V3" H 5715 3473 50  0000 C CNN
F 2 "" H 5700 3300 50  0001 C CNN
F 3 "" H 5700 3300 50  0001 C CNN
	1    5700 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 3550 5700 3550
Wire Wire Line
	5450 3750 5450 4200
Wire Wire Line
	5450 4200 4150 4200
Wire Wire Line
	4150 4200 4150 3350
Wire Wire Line
	4150 3350 4450 3350
Wire Wire Line
	5700 3600 5700 3550
Wire Wire Line
	5700 3550 5700 3500
Connection ~ 5700 3550
Wire Wire Line
	5550 3850 5700 3850
Wire Wire Line
	5550 4000 5550 3850
$Comp
L Device:R_POT_US RV?
U 1 1 5FB17A8F
P 5700 4000
AR Path="/5FB17A8F" Ref="RV?"  Part="1" 
AR Path="/5FAFFE1A/5FB17A8F" Ref="RV1"  Part="1" 
F 0 "RV1" H 5632 3954 50  0000 R CNN
F 1 "100k" H 5632 4045 50  0000 R CNN
F 2 "" H 5700 4000 50  0001 C CNN
F 3 "~" H 5700 4000 50  0001 C CNN
	1    5700 4000
	-1   0    0    1   
$EndComp
Wire Wire Line
	5450 4200 5700 4200
Wire Wire Line
	5700 4200 5700 4150
Wire Wire Line
	5700 3850 5700 3800
Connection ~ 5700 3850
Connection ~ 5450 4200
$Comp
L power:GNDREF #PWR?
U 1 1 5FB17A9A
P 5700 4450
AR Path="/5FB17A9A" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17A9A" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 5700 4200 50  0001 C CNN
F 1 "GNDREF" H 5705 4277 50  0001 C CNN
F 2 "" H 5700 4450 50  0001 C CNN
F 3 "" H 5700 4450 50  0001 C CNN
	1    5700 4450
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5FB17AA0
P 4300 3700
AR Path="/5FB17AA0" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17AA0" Ref="#PWR012"  Part="1" 
F 0 "#PWR012" H 4300 3550 50  0001 C CNN
F 1 "+3V3" H 4315 3873 50  0000 C CNN
F 2 "" H 4300 3700 50  0001 C CNN
F 3 "" H 4300 3700 50  0001 C CNN
	1    4300 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3700 4300 3750
Wire Wire Line
	4300 3750 4450 3750
Wire Wire Line
	5700 4250 5700 4200
Connection ~ 5700 4200
$Comp
L power:GNDREF #PWR?
U 1 1 5FB17AAA
P 4400 2750
AR Path="/5FB17AAA" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17AAA" Ref="#PWR014"  Part="1" 
F 0 "#PWR014" H 4400 2500 50  0001 C CNN
F 1 "GNDREF" H 4405 2577 50  0001 C CNN
F 2 "" H 4400 2750 50  0001 C CNN
F 3 "" H 4400 2750 50  0001 C CNN
	1    4400 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 2550 4400 2500
Wire Wire Line
	4400 2500 4250 2500
Wire Wire Line
	4250 2500 4250 2400
Connection ~ 4400 2500
Wire Wire Line
	4250 2200 4250 2100
Wire Wire Line
	4250 2100 4400 2100
$Comp
L power:+3V3 #PWR?
U 1 1 5FB17AB6
P 4400 2050
AR Path="/5FB17AB6" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17AB6" Ref="#PWR013"  Part="1" 
F 0 "#PWR013" H 4400 1900 50  0001 C CNN
F 1 "+3V3" H 4415 2223 50  0000 C CNN
F 2 "" H 4400 2050 50  0001 C CNN
F 3 "" H 4400 2050 50  0001 C CNN
	1    4400 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 2100 4400 2050
Connection ~ 4400 2100
Wire Wire Line
	4400 2500 4850 2500
$Comp
L Device:R_Small_US R?
U 1 1 5FB17ADA
P 7100 4300
AR Path="/5FB17ADA" Ref="R?"  Part="1" 
AR Path="/5FAFFE1A/5FB17ADA" Ref="R8"  Part="1" 
F 0 "R8" H 7150 4350 50  0000 L CNN
F 1 "1k" H 7150 4250 50  0000 L CNN
F 2 "" H 7100 4300 50  0001 C CNN
F 3 "~" H 7100 4300 50  0001 C CNN
	1    7100 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5FB17AE0
P 7400 4150
AR Path="/5FB17AE0" Ref="C?"  Part="1" 
AR Path="/5FAFFE1A/5FB17AE0" Ref="C15"  Part="1" 
F 0 "C15" V 7450 4200 50  0000 L CNN
F 1 "100n" V 7450 3900 50  0000 L CNN
F 2 "" H 7400 4150 50  0001 C CNN
F 3 "~" H 7400 4150 50  0001 C CNN
	1    7400 4150
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_DIP_x02 SW?
U 1 1 5FB17AE6
P 7400 3850
AR Path="/5FB17AE6" Ref="SW?"  Part="1" 
AR Path="/5FAFFE1A/5FB17AE6" Ref="SW3"  Part="1" 
F 0 "SW3" H 7400 4100 50  0000 C CNN
F 1 "CLK_SEL" H 7400 3700 50  0000 C CNN
F 2 "" H 7400 3850 50  0001 C CNN
F 3 "~" H 7400 3850 50  0001 C CNN
	1    7400 3850
	1    0    0    -1  
$EndComp
Connection ~ 5450 1200
Wire Wire Line
	5450 1200 5450 1300
Wire Wire Line
	5500 1200 5450 1200
$Comp
L power:GNDREF #PWR?
U 1 1 5FB17AEF
P 5700 1200
AR Path="/5FB17AEF" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17AEF" Ref="#PWR025"  Part="1" 
F 0 "#PWR025" H 5700 950 50  0001 C CNN
F 1 "GNDREF" H 5705 1027 50  0001 C CNN
F 2 "" H 5700 1200 50  0001 C CNN
F 3 "" H 5700 1200 50  0001 C CNN
	1    5700 1200
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5FB17AF5
P 5450 1200
AR Path="/5FB17AF5" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17AF5" Ref="#PWR023"  Part="1" 
F 0 "#PWR023" H 5450 1050 50  0001 C CNN
F 1 "+3V3" H 5465 1373 50  0000 C CNN
F 2 "" H 5450 1200 50  0001 C CNN
F 3 "" H 5450 1200 50  0001 C CNN
	1    5450 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5FB17AFB
P 5600 1200
AR Path="/5FB17AFB" Ref="C?"  Part="1" 
AR Path="/5FAFFE1A/5FB17AFB" Ref="C11"  Part="1" 
F 0 "C11" V 5500 1100 50  0000 L CNN
F 1 "100n" V 5550 1250 50  0000 L CNN
F 2 "" H 5600 1200 50  0001 C CNN
F 3 "~" H 5600 1200 50  0001 C CNN
	1    5600 1200
	0    1    1    0   
$EndComp
$Comp
L power:GNDREF #PWR?
U 1 1 5FB17B01
P 5450 1500
AR Path="/5FB17B01" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B01" Ref="#PWR024"  Part="1" 
F 0 "#PWR024" H 5450 1250 50  0001 C CNN
F 1 "GNDREF" H 5455 1327 50  0001 C CNN
F 2 "" H 5450 1500 50  0001 C CNN
F 3 "" H 5450 1500 50  0001 C CNN
	1    5450 1500
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5FB17B07
P 4700 1050
AR Path="/5FB17B07" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B07" Ref="#PWR015"  Part="1" 
F 0 "#PWR015" H 4700 900 50  0001 C CNN
F 1 "+3V3" H 4715 1223 50  0000 C CNN
F 2 "" H 4700 1050 50  0001 C CNN
F 3 "" H 4700 1050 50  0001 C CNN
	1    4700 1050
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR?
U 1 1 5FB17B0D
P 4700 1750
AR Path="/5FB17B0D" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B0D" Ref="#PWR016"  Part="1" 
F 0 "#PWR016" H 4700 1500 50  0001 C CNN
F 1 "GNDREF" H 4705 1577 50  0001 C CNN
F 2 "" H 4700 1750 50  0001 C CNN
F 3 "" H 4700 1750 50  0001 C CNN
	1    4700 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 1400 4700 1350
Wire Wire Line
	4700 1400 4600 1400
Connection ~ 4700 1400
Wire Wire Line
	4700 1450 4700 1400
Wire Wire Line
	5150 1400 4700 1400
Wire Wire Line
	4400 1400 4300 1400
$Comp
L Device:R_Small_US R?
U 1 1 5FB17B19
P 4500 1400
AR Path="/5FB17B19" Ref="R?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B19" Ref="R4"  Part="1" 
F 0 "R4" V 4400 1350 50  0000 L CNN
F 1 "1k" V 4600 1350 50  0000 L CNN
F 2 "" H 4500 1400 50  0001 C CNN
F 3 "~" H 4500 1400 50  0001 C CNN
	1    4500 1400
	0    1    1    0   
$EndComp
$Comp
L power:GNDREF #PWR?
U 1 1 5FB17B1F
P 4100 1600
AR Path="/5FB17B1F" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B1F" Ref="#PWR011"  Part="1" 
F 0 "#PWR011" H 4100 1350 50  0001 C CNN
F 1 "GNDREF" H 4105 1427 50  0001 C CNN
F 2 "" H 4100 1600 50  0001 C CNN
F 3 "" H 4100 1600 50  0001 C CNN
	1    4100 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5FB17B25
P 4700 1600
AR Path="/5FB17B25" Ref="D?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B25" Ref="D2"  Part="1" 
F 0 "D2" H 4700 1500 50  0000 C CNN
F 1 "BAT54TA" H 4700 1700 50  0000 C CNN
F 2 "" H 4700 1600 50  0001 C CNN
F 3 "~" H 4700 1600 50  0001 C CNN
	1    4700 1600
	0    1    1    0   
$EndComp
$Comp
L Device:D_Schottky D?
U 1 1 5FB17B2B
P 4700 1200
AR Path="/5FB17B2B" Ref="D?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B2B" Ref="D1"  Part="1" 
F 0 "D1" H 4700 1100 50  0000 C CNN
F 1 "BAT54TA" H 4700 1300 50  0000 C CNN
F 2 "" H 4700 1200 50  0001 C CNN
F 3 "~" H 4700 1200 50  0001 C CNN
	1    4700 1200
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_Coaxial J?
U 1 1 5FB17B31
P 4100 1400
AR Path="/5FB17B31" Ref="J?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B31" Ref="J2"  Part="1" 
F 0 "J2" H 3950 1300 50  0000 L CNN
F 1 "EXT_CLK" H 3850 1550 50  0000 L CNN
F 2 "" H 4100 1400 50  0001 C CNN
F 3 " ~" H 4100 1400 50  0001 C CNN
	1    4100 1400
	-1   0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5FB17B3D
P 7700 3450
AR Path="/5FB17B3D" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B3D" Ref="#PWR034"  Part="1" 
F 0 "#PWR034" H 7700 3300 50  0001 C CNN
F 1 "+3V3" H 7715 3623 50  0000 C CNN
F 2 "" H 7700 3450 50  0001 C CNN
F 3 "" H 7700 3450 50  0001 C CNN
	1    7700 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 3750 7700 3850
$Comp
L Device:C_Small C?
U 1 1 5FB17B44
P 7400 3450
AR Path="/5FB17B44" Ref="C?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B44" Ref="C14"  Part="1" 
F 0 "C14" V 7300 3350 50  0000 L CNN
F 1 "100n" V 7350 3500 50  0000 L CNN
F 2 "" H 7400 3450 50  0001 C CNN
F 3 "~" H 7400 3450 50  0001 C CNN
	1    7400 3450
	0    1    1    0   
$EndComp
Wire Wire Line
	7500 3450 7700 3450
Wire Wire Line
	7700 3450 7700 3750
Connection ~ 7700 3450
Connection ~ 7700 3750
Wire Wire Line
	7500 4150 7700 4150
Wire Wire Line
	7700 4150 7700 3850
Connection ~ 7700 3850
Wire Wire Line
	7300 3450 7100 3450
Wire Wire Line
	7100 3450 7100 3550
$Comp
L power:GNDREF #PWR?
U 1 1 5FB17B54
P 7100 4400
AR Path="/5FB17B54" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B54" Ref="#PWR030"  Part="1" 
F 0 "#PWR030" H 7100 4150 50  0001 C CNN
F 1 "GNDREF" H 7105 4227 50  0001 C CNN
F 2 "" H 7100 4400 50  0001 C CNN
F 3 "" H 7100 4400 50  0001 C CNN
	1    7100 4400
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 5FB17B5E
P 6950 4300
AR Path="/5FB17B5E" Ref="R?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B5E" Ref="R7"  Part="1" 
F 0 "R7" H 6800 4350 50  0000 L CNN
F 1 "1k" H 6800 4250 50  0000 L CNN
F 2 "" H 6950 4300 50  0001 C CNN
F 3 "~" H 6950 4300 50  0001 C CNN
	1    6950 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR?
U 1 1 5FB17B64
P 6950 4400
AR Path="/5FB17B64" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B64" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 6950 4150 50  0001 C CNN
F 1 "GNDREF" H 6955 4227 50  0001 C CNN
F 2 "" H 6950 4400 50  0001 C CNN
F 3 "" H 6950 4400 50  0001 C CNN
	1    6950 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 4200 6950 3750
Wire Wire Line
	6950 3750 7100 3750
Connection ~ 7100 3750
Wire Wire Line
	6050 2800 6150 2800
$Comp
L power:GNDREF #PWR?
U 1 1 5FB17B76
P 6650 3200
AR Path="/5FB17B76" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B76" Ref="#PWR032"  Part="1" 
F 0 "#PWR032" H 6650 2950 50  0001 C CNN
F 1 "GNDREF" H 6655 3027 50  0001 C CNN
F 2 "" H 6650 3200 50  0001 C CNN
F 3 "" H 6650 3200 50  0001 C CNN
	1    6650 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5FB17B7C
P 6800 1050
AR Path="/5FB17B7C" Ref="C?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B7C" Ref="C13"  Part="1" 
F 0 "C13" V 6700 950 50  0000 L CNN
F 1 "100n" V 6750 1100 50  0000 L CNN
F 2 "" H 6800 1050 50  0001 C CNN
F 3 "~" H 6800 1050 50  0001 C CNN
	1    6800 1050
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5FB17B82
P 6650 1050
AR Path="/5FB17B82" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B82" Ref="#PWR031"  Part="1" 
F 0 "#PWR031" H 6650 900 50  0001 C CNN
F 1 "+3V3" H 6665 1223 50  0000 C CNN
F 2 "" H 6650 1050 50  0001 C CNN
F 3 "" H 6650 1050 50  0001 C CNN
	1    6650 1050
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR?
U 1 1 5FB17B88
P 6900 1050
AR Path="/5FB17B88" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B88" Ref="#PWR033"  Part="1" 
F 0 "#PWR033" H 6900 800 50  0001 C CNN
F 1 "GNDREF" H 6905 877 50  0001 C CNN
F 2 "" H 6900 1050 50  0001 C CNN
F 3 "" H 6900 1050 50  0001 C CNN
	1    6900 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 1050 6650 1050
Wire Wire Line
	6650 1100 6650 1050
Connection ~ 6650 1050
$Comp
L power:GNDREF #PWR?
U 1 1 5FB17B91
P 6150 2600
AR Path="/5FB17B91" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FB17B91" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 6150 2350 50  0001 C CNN
F 1 "GNDREF" H 6155 2427 50  0001 C CNN
F 2 "" H 6150 2600 50  0001 C CNN
F 3 "" H 6150 2600 50  0001 C CNN
	1    6150 2600
	1    0    0    -1  
$EndComp
NoConn ~ 7150 2100
Wire Wire Line
	5400 2500 5700 2500
Wire Wire Line
	5700 2500 5700 1600
Wire Wire Line
	5450 3350 5550 3350
Wire Wire Line
	5550 2650 5800 2650
Wire Wire Line
	5800 2650 5800 1700
Connection ~ 6150 2600
Wire Wire Line
	6150 2600 6150 2400
Connection ~ 6150 2400
Wire Wire Line
	6150 2400 6150 2300
Connection ~ 6150 2300
Wire Wire Line
	6150 2200 6150 2100
Wire Wire Line
	6150 2300 6150 2200
Connection ~ 6150 2200
$Comp
L 74xx:74LS253 U?
U 1 1 5FB17BA9
P 6650 2100
AR Path="/5FB17BA9" Ref="U?"  Part="1" 
AR Path="/5FAFFE1A/5FB17BA9" Ref="U22"  Part="1" 
F 0 "U22" H 6400 2950 50  0000 C CNN
F 1 "SN74HC253" V 6650 2100 50  0000 C CNN
F 2 "" H 6650 2100 50  0001 C CNN
F 3 "https://www.digikey.com/product-detail/en/texas-instruments/SN74HC253DBR/296-8282-1-ND/376639" H 6650 2100 50  0001 C CNN
	1    6650 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 2100 6150 1900
Connection ~ 6150 2100
Text HLabel 7150 1400 2    50   Input ~ 0
clk
$Comp
L power:GNDREF #PWR?
U 1 1 60674FA2
P 6800 5300
AR Path="/60674FA2" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/60674FA2" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 6800 5050 50  0001 C CNN
F 1 "GNDREF" H 6805 5127 50  0001 C CNN
F 2 "" H 6800 5300 50  0001 C CNN
F 3 "" H 6800 5300 50  0001 C CNN
	1    6800 5300
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 60674FA8
P 6800 5000
AR Path="/60674FA8" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/60674FA8" Ref="#PWR06"  Part="1" 
F 0 "#PWR06" H 6800 4850 50  0001 C CNN
F 1 "+3V3" H 6815 5173 50  0000 C CNN
F 2 "" H 6800 5000 50  0001 C CNN
F 3 "" H 6800 5000 50  0001 C CNN
	1    6800 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60674FAE
P 6950 5000
AR Path="/60674FAE" Ref="C?"  Part="1" 
AR Path="/5FAFFE1A/60674FAE" Ref="C5"  Part="1" 
F 0 "C5" V 6850 4950 50  0000 L CNN
F 1 "100n" V 6900 5050 50  0000 L CNN
F 2 "" H 6950 5000 50  0001 C CNN
F 3 "~" H 6950 5000 50  0001 C CNN
	1    6950 5000
	0    1    1    0   
$EndComp
$Comp
L power:GNDREF #PWR?
U 1 1 60674FB4
P 7050 5000
AR Path="/60674FB4" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/60674FB4" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 7050 4750 50  0001 C CNN
F 1 "GNDREF" H 7055 4827 50  0001 C CNN
F 2 "" H 7050 5000 50  0001 C CNN
F 3 "" H 7050 5000 50  0001 C CNN
	1    7050 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 5000 6800 5000
Wire Wire Line
	6800 5000 6800 5100
Connection ~ 6800 5000
$Comp
L Switch:SW_Push SW?
U 1 1 60674FBD
P 6300 5400
AR Path="/60674FBD" Ref="SW?"  Part="1" 
AR Path="/5FAFFE1A/60674FBD" Ref="SW1"  Part="1" 
F 0 "SW1" V 6200 5500 50  0000 L CNN
F 1 "RST Button" H 6150 5300 50  0000 L CNN
F 2 "" H 6300 5600 50  0001 C CNN
F 3 "~" H 6300 5600 50  0001 C CNN
	1    6300 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	6300 5200 6500 5200
Wire Wire Line
	7050 5200 7200 5200
$Comp
L Device:C_Small C?
U 1 1 60674FC6
P 5950 5350
AR Path="/60674FC6" Ref="C?"  Part="1" 
AR Path="/5FAFFE1A/60674FC6" Ref="C1"  Part="1" 
F 0 "C1" H 6000 5450 50  0000 L CNN
F 1 "4.7u" V 6050 5200 50  0000 L CNN
F 2 "" H 5950 5350 50  0001 C CNN
F 3 "~" H 5950 5350 50  0001 C CNN
	1    5950 5350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 60674FCC
P 5750 5050
AR Path="/60674FCC" Ref="R?"  Part="1" 
AR Path="/5FAFFE1A/60674FCC" Ref="R1"  Part="1" 
F 0 "R1" H 5818 5096 50  0000 L CNN
F 1 "15k" H 5818 5005 50  0000 L CNN
F 2 "" H 5750 5050 50  0001 C CNN
F 3 "~" H 5750 5050 50  0001 C CNN
	1    5750 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R?
U 1 1 60674FD2
P 5750 5350
AR Path="/60674FD2" Ref="R?"  Part="1" 
AR Path="/5FAFFE1A/60674FD2" Ref="R2"  Part="1" 
F 0 "R2" H 5600 5400 50  0000 L CNN
F 1 "47k" H 5550 5300 50  0000 L CNN
F 2 "" H 5750 5350 50  0001 C CNN
F 3 "~" H 5750 5350 50  0001 C CNN
	1    5750 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 5250 5950 5200
Wire Wire Line
	5950 5200 6300 5200
Connection ~ 6300 5200
Wire Wire Line
	5750 5150 5750 5200
Wire Wire Line
	5750 5200 5950 5200
Connection ~ 5750 5200
Wire Wire Line
	5750 5200 5750 5250
Connection ~ 5950 5200
$Comp
L power:GNDREF #PWR?
U 1 1 60674FE0
P 6300 5600
AR Path="/60674FE0" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/60674FE0" Ref="#PWR05"  Part="1" 
F 0 "#PWR05" H 6300 5350 50  0001 C CNN
F 1 "GNDREF" H 6305 5427 50  0001 C CNN
F 2 "" H 6300 5600 50  0001 C CNN
F 3 "" H 6300 5600 50  0001 C CNN
	1    6300 5600
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 60674FE6
P 5750 4950
AR Path="/60674FE6" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/60674FE6" Ref="#PWR02"  Part="1" 
F 0 "#PWR02" H 5750 4800 50  0001 C CNN
F 1 "+3V3" H 5765 5123 50  0000 C CNN
F 2 "" H 5750 4950 50  0001 C CNN
F 3 "" H 5750 4950 50  0001 C CNN
	1    5750 4950
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR?
U 1 1 60674FEC
P 5950 5450
AR Path="/60674FEC" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/60674FEC" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 5950 5200 50  0001 C CNN
F 1 "GNDREF" H 5955 5277 50  0001 C CNN
F 2 "" H 5950 5450 50  0001 C CNN
F 3 "" H 5950 5450 50  0001 C CNN
	1    5950 5450
	1    0    0    -1  
$EndComp
$Comp
L power:GNDREF #PWR?
U 1 1 60674FF2
P 5750 5450
AR Path="/60674FF2" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/60674FF2" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 5750 5200 50  0001 C CNN
F 1 "GNDREF" H 5755 5277 50  0001 C CNN
F 2 "" H 5750 5450 50  0001 C CNN
F 3 "" H 5750 5450 50  0001 C CNN
	1    5750 5450
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74LVC3G17 U1
U 2 1 6068E4D8
P 5150 2500
F 0 "U1" H 5100 2350 50  0000 C CNN
F 1 "SN74LVC3G17" H 5100 2650 50  0000 C CNN
F 2 "" H 5150 2500 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 5150 2500 50  0001 C CNN
	2    5150 2500
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74LVC3G17 U1
U 1 1 6068F356
P 5450 1400
F 0 "U1" H 5300 1250 50  0000 C CNN
F 1 "SN74LVC3G17" H 5150 1550 50  0000 C CNN
F 2 "" H 5450 1400 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 5450 1400 50  0001 C CNN
	1    5450 1400
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74LVC3G17 U1
U 3 1 6069030A
P 5550 2900
F 0 "U1" H 5650 2800 50  0000 C CNN
F 1 "SN74LVC3G17" H 5550 3050 50  0000 C CNN
F 2 "" H 5550 2900 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 5550 2900 50  0001 C CNN
	3    5550 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5550 3350 5550 3200
$Comp
L 74xGxx:74LVC3G17 U20
U 3 1 6068C6F9
P 6450 3850
F 0 "U20" H 6400 3700 50  0000 C CNN
F 1 "SN74LVC3G17" H 6400 4000 50  0001 C CNN
F 2 "" H 6450 3850 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 6450 3850 50  0001 C CNN
	3    6450 3850
	-1   0    0    -1  
$EndComp
$Comp
L 74xGxx:74LVC3G17 U20
U 1 1 6068D68D
P 6800 5200
F 0 "U20" H 6650 5050 50  0000 C CNN
F 1 "SN74LVC3G17" H 6500 5350 50  0000 C CNN
F 2 "" H 6800 5200 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 6800 5200 50  0001 C CNN
	1    6800 5200
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74LVC3G17 U20
U 2 1 6068B78E
P 6450 3550
F 0 "U20" H 6400 3400 50  0000 C CNN
F 1 "SN74LVC3G17" H 6400 3700 50  0000 C CNN
F 2 "" H 6450 3550 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 6450 3550 50  0001 C CNN
	2    6450 3550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6050 3850 6200 3850
Wire Wire Line
	6050 2800 6050 3850
Wire Wire Line
	6150 3550 6200 3550
Wire Wire Line
	6150 2900 6150 3550
Wire Wire Line
	7300 4150 7100 4150
Wire Wire Line
	7100 4200 7100 4150
Wire Wire Line
	7100 4150 7100 3850
Connection ~ 7100 4150
Wire Wire Line
	7100 3850 6750 3850
Connection ~ 7100 3850
Wire Wire Line
	6750 3550 7100 3550
Connection ~ 7100 3550
Wire Wire Line
	7100 3550 7100 3750
Text HLabel 7200 5200 2    50   Input ~ 0
~rst
Wire Wire Line
	5800 1700 6150 1700
Wire Wire Line
	6150 1600 5700 1600
Wire Wire Line
	5700 1400 5800 1400
Wire Wire Line
	5800 1400 5800 1500
Wire Wire Line
	5800 1500 6150 1500
$Comp
L power:GNDREF #PWR?
U 1 1 5FA57D64
P 6050 1350
AR Path="/5FA57D64" Ref="#PWR?"  Part="1" 
AR Path="/5FAFFE1A/5FA57D64" Ref="#PWR?"  Part="1" 
F 0 "#PWR?" H 6050 1100 50  0001 C CNN
F 1 "GNDREF" H 6055 1177 50  0001 C CNN
F 2 "" H 6050 1350 50  0001 C CNN
F 3 "" H 6050 1350 50  0001 C CNN
	1    6050 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 1350 6150 1350
Wire Wire Line
	6150 1350 6150 1400
$EndSCHEMATC
