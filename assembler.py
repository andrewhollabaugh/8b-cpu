#!/usr/bin/env python3

import os
import sys

comment_char = '#'
i_len = 3
bus_addr_len = 1

#instruction types:
#    0: no arguments
#    1: bus_addr argument only
#    2: immediate argument only
#    3: bus_addr and immediate arguments

i_dict = {   #i_type    opcode \
        "nop":  [0, "0000000000000000"], \
        "ldi":  [3, "101000"], \
        "ldm":  [1, "00110000000000"], \
        "add":  [1, "00101100000000"], \
        "sub":  [1, "00101110000000"], \
        "and":  [1, "00100000000000"], \
        "ior":  [1, "00100100000000"], \
        "xor":  [1, "00101000000000"], \
        "buc":  [2, "110100"], \
        "beq":  [2, "110000"], \
        "bne":  [2, "110001"], \
        "blt":  [2, "110010"], \
        "bgt":  [2, "110011"]}

bus_addr_dict = { \
        "a": "00", \
        "b": "01", \
        "d": "10", \
        "m": "11"} \

output = []

try:
    asm_file_path = sys.argv[1]
except:
    print("error: need to specify ASM file location")
    quit()

# open the assembly file and copy to a list
asm_line_list = []
with open(asm_file_path, 'r') as asm_file:
    asm_line_list = asm_file.readlines()

line_num = 0
for line in asm_line_list:

    #remove comments
    comment_pos = line.find(comment_char)
    if comment_pos != -1:
        line = line[:comment_pos]

    #find the instruction in the line
    i = ""
    for i_itr in i_dict:
        if i_itr == line[:i_len]:
            i = i_itr
            break

    if i != "": #an instruction was found
        i_type = i_dict.get(i)[0]
        opcode = i_dict.get(i)[1]
        bus_addr_bits = ""
        im_bits = ""

        if i_type == 1 or i_type == 3: #instruction with a bus_addr argument
            bus_addr = ""
            for bus_addr_itr in bus_addr_dict:
                if bus_addr_itr == line[4]:
                    bus_addr = bus_addr_itr

            if bus_addr == "": 
                print("syntax error on line " + str(line_num))
            else: #a bus_addr was found
                bus_addr_bits = bus_addr_dict.get(bus_addr)
        elif i_type == 2:
            bus_addr_bits = "00"

        if i_type == 2 or i_type == 3: #instruction with a immediate argument
            if i_type == 2:
                im_str = line[4:6]
            elif i_type == 3:
                im_str = line[6:8]
            im_bits = bin(int(im_str, 16))[2:].zfill(8)

        output.append(opcode + im_bits + bus_addr_bits)

    line_num = line_num + 1

for line in output:
    print(line)

print("\n\n==FOR VERILOG===")
line_num = 0
for line in output:
    print("8'h" + hex(int(str(line_num), 16))[2:].zfill(2) + ": I <= 16'b" + line + ";")
    line_num = line_num + 1

