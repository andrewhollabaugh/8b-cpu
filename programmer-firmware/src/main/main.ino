
#include "main.h"
#include "uart.h"
#include "queue.h"
#include "mcp23s17.h"
#include "pinout.h"

void setup()
{
    uart_init();

    //setup printf
    //FILE uart_output = FDEV_SETUP_STREAM(&uart_tx_putchar, NULL, _FDEV_SETUP_WRITE);
    //stdout = &uart_output;

	ioexp_init();
	IOEXP_SET_ALL_INPUT();

	SET_PIN_LOW(LED0_PORT, LED0_PIN);
	SET_PIN_OUTPUT(LED0_DDR, LED0_PIN);

	SET_PIN_LOW(LED1_PORT, LED1_PIN);
	SET_PIN_OUTPUT(LED1_DDR, LED1_PIN);

	SET_PIN_HIGH(BUTTON_PORT, BUTTON_PIN); //enable pullup
	SET_PIN_INPUT(BUTTON_DDR, BUTTON_PIN);

	SET_PIN_LOW(CE_PORT, CE_PIN);
	SET_PIN_OUTPUT(CE_DDR, CE_PIN);

	SET_PIN_HIGH(WE_PORT, WE_PIN);
	SET_PIN_OUTPUT(WE_DDR, WE_PIN);

	SET_PIN_LOW(OE_PORT, OE_PIN);
	SET_PIN_OUTPUT(OE_DDR, OE_PIN);

	SET_PIN_LOW(ASEL_PORT, ASEL_PIN);
	SET_PIN_OUTPUT(ASEL_DDR, ASEL_PIN);

	SET_PIN_OUTPUT(A0_DDR, A0_PIN);
	SET_PIN_OUTPUT(A1_DDR, A1_PIN);
	SET_PIN_OUTPUT(A2_DDR, A2_PIN);
	SET_PIN_OUTPUT(A3_DDR, A3_PIN);
	SET_PIN_OUTPUT(A4_DDR, A4_PIN);
	SET_PIN_OUTPUT(A5_DDR, A5_PIN);
	SET_PIN_OUTPUT(A6_DDR, A6_PIN);
	SET_PIN_OUTPUT(A7_DDR, A7_PIN);

	sei();
}

void loop()
{
    if(!queue_is_empty())
	    on_uart_rx(queue_pop());
}

void on_uart_rx(uint8_t rx_byte)
{
	SET_PIN_HIGH(LED0_PORT, LED0_PIN);

	if(!is_write_rx)
	{
		if(rx_byte == WRITE_START_BYTE)
		{
			buf_addr = 0;
			current_byte_pos = BP_MSB;
			is_write_rx = 1;
		}
		else if(rx_byte == READ_BYTE)
		{
			rw_flash(0);
			current_byte_pos = BP_MSB;
			uint16_t addr = 0;
			while (addr < FLASH_SIZE)
			{
				if (current_byte_pos == BP_MSB)
				{
					uart_tx((uint8_t)(flash_buf[addr] >> 8U));
					current_byte_pos = BP_LSB;
				} else if (current_byte_pos == BP_LSB)
				{
					uart_tx((uint8_t)(flash_buf[addr]));
					current_byte_pos = BP_MSB;
					addr++;
					TOGGLE_PIN(LED1_PORT, LED1_PIN);
				}
			}
			uart_tx(END_BYTE);
		}
	}
	else
	{
		if(rx_byte == END_BYTE)
		{
			if(current_byte_pos == BP_MSB && buf_addr == FLASH_SIZE)
			{
				rw_flash(1);
				is_write_rx = 0;
			}
			else
			{
				error_num_bytes_rx();
			}
		}
		else
		{
			if(buf_addr > FLASH_SIZE - 1) //array out of index
			{
				error_num_bytes_rx();
			}
			else
			{
				if(current_byte_pos == BP_MSB)
				{
					flash_buf[buf_addr] |= (uint16_t)(rx_byte << 8U);
					current_byte_pos = BP_LSB;
				}
				else
				{
					flash_buf[buf_addr] |= (uint16_t)(rx_byte);
					current_byte_pos = BP_MSB;
					buf_addr++;
				}
			}
		}
	}

	SET_PIN_LOW(LED0_PORT, LED0_PIN);
}

void error_num_bytes_rx()
{
	printf("error: num bytes rx\n");
	is_write_rx = 0;
}

void rw_flash(uint8_t write)
{
	IOEXP_SET_ALL_INPUT();
	SET_PIN_HIGH(OE_PORT, OE_PIN);
	SET_PIN_HIGH(WE_PORT, WE_PIN);
	SET_PIN_HIGH(ASEL_PORT, ASEL_PIN);

	addr_pin_t addr_pins[NUM_ADDR_PINS] = {
			{A0_PORT, A0_PIN},
			{A1_PORT, A1_PIN},
			{A2_PORT, A2_PIN},
			{A3_PORT, A3_PIN},
			{A4_PORT, A4_PIN},
			{A5_PORT, A5_PIN},
			{A6_PORT, A6_PIN},
			{A7_PORT, A7_PIN}
	};

	for(uint16_t addr = 0; addr < FLASH_SIZE; addr++)
	{
		for(uint8_t addr_pin_num = 0; addr_pin_num < NUM_ADDR_PINS; addr_pin_num++)
		{
			uint8_t addr_pin_state = (addr >> addr_pin_num) & 0x01;
			addr_pin_t addr_pin = addr_pins[addr_pin_num];
			if(addr_pin_state == 0)
				SET_PIN_LOW(addr_pin.port, addr_pin.pin);
			else if(addr_pin_state == 1)
				SET_PIN_HIGH(addr_pin.port, addr_pin.pin);
		}

		if(write)
		{
			IOEXP_SET_ALL_OUTPUT();
			ioexp_write_gpio(flash_buf[addr]);
			SET_PIN_LOW(WE_PORT, WE_PIN);
			_delay_ms(1);
			SET_PIN_HIGH(WE_PORT, WE_PIN);
			_delay_ms(1);
		}
		else //read
		{
			IOEXP_SET_ALL_INPUT();
			SET_PIN_LOW(OE_PORT, OE_PIN);
			_delay_ms(1);
			flash_buf[addr] = ioexp_read_gpio();
			SET_PIN_HIGH(OE_PORT, OE_PIN);
			_delay_ms(1);
		}

		TOGGLE_PIN(LED1_PORT, LED1_PIN);
	}

	IOEXP_SET_ALL_INPUT();
	SET_PIN_HIGH(WE_PORT, WE_PIN);
	SET_PIN_HIGH(OE_PORT, OE_PIN);
	SET_PIN_LOW(ASEL_PORT, ASEL_PIN);
}
