#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <util/delay.h>
#include "pinout.h"
#include <Arduino.h>

#define WRITE_START_BYTE 0x55
#define END_BYTE 0x7E
#define READ_BYTE 0x66

#define FLASH_SIZE 256
#define NUM_ADDR_PINS 8

uint8_t is_write_rx = 0; //is it expecting to receive flash data over uart
uint16_t buf_addr = 0;

typedef enum {
    BP_MSB,
    BP_LSB
} byte_pos;
uint8_t current_byte_pos = BP_MSB;

uint16_t flash_buf[FLASH_SIZE];

typedef struct {
	uint8_t port;
	uint8_t pin;
} addr_pin_t;

int main();
void on_uart_rx(uint8_t rx_byte);
void error_num_bytes_rx();
void rw_flash(uint8_t write);

#endif
