
#include "spi.h"
#include "pinout.h"

void spi_init()
{
	SET_PIN_OUTPUT(MOSI_DDR, MOSI_PIN);
	SET_PIN_INPUT(MISO_DDR, MISO_PIN);
	SET_PIN_OUTPUT(SCK_DDR, SCK_PIN);

	SET_PIN_HIGH(SS_PORT, SS_PIN);
	SET_PIN_OUTPUT(SS_DDR, SS_PIN);

	SPCR |= (1U << SPE); //enable SPI
	SPCR &= ~(1U << DORD); //MSB first
	SPCR |= (1U << MSTR); //master mode
	SPCR &= ~(1U << CPOL); //normal polarity
	SPCR &= ~(1U << CPHA); //rising edge data transfer

	//SCK freq: fosc / 16
	SPSR &= ~(1U << SPI2X);
	SPCR &= ~(1U << SPR1);
	SPCR |= (1U << SPR0);
}

uint8_t spi_mstr_tx_rx(uint8_t data)
{
	SPDR = data;
	while(~SPSR & (1U << SPIF));
	return SPDR;
}
