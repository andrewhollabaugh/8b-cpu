#ifndef SPI_H
#define SPI_H

#include <stdint.h>
#include <avr/io.h>

#define SS_DDR IOSS_DDR
#define SS_PORT IOSS_PORT
#define SS_PIN IOSS_PIN

void spi_init();
uint8_t spi_mstr_tx_rx(uint8_t data);

#endif
