#ifndef PINOUT_H
#define PINOUT_H

#include <avr/io.h>

#define SET_PIN_INPUT(DDR, PIN) ((DDR) &= ~(1U << (PIN)))
#define SET_PIN_OUTPUT(DDR, PIN) ((DDR) |= (1U << (PIN)))
#define SET_PIN_LOW(PORT, PIN) ((PORT) &= ~(1U << (PIN)))
#define SET_PIN_HIGH(PORT, PIN) ((PORT) |= (1U << (PIN)))
#define TOGGLE_PIN(PORT, PIN) ((PORT) ^= (1U << (PIN)))

#define LED0_DDR DDRD
#define LED0_PORT PORTD
#define LED0_PIN 5U

#define LED1_DDR DDRB
#define LED1_PORT PORTB
#define LED1_PIN 7U

#define BUTTON_DDR DDRD
#define BUTTON_PORT PORTD
#define BUTTON_PINR PIND
#define BUTTON_PIN 2U

#define CE_DDR DDRD
#define CE_PORT PORTD
#define CE_PIN 6U

#define WE_DDR DDRD
#define WE_PORT PORTD
#define WE_PIN 7U

#define OE_DDR DDRB
#define OE_PORT PORTB
#define OE_PIN 0U

#define ASEL_DDR DDRB
#define ASEL_PORT PORTB
#define ASEL_PIN 1U

#define MOSI_DDR DDRB
#define MOSI_PORT PORTB
#define MOSI_PIN 3U

#define MISO_DDR DDRB
#define MISO_PORT PORTB
#define MISO_PIN 4U

#define SCK_DDR DDRB
#define SCK_PORT PORTB
#define SCK_PIN 5U

#define IOSS_DDR DDRB
#define IOSS_PORT PORTB
#define IOSS_PIN 2U

#define A0_DDR DDRD
#define A0_PORT PORTD
#define A0_PIN 3U

#define A1_DDR DDRD
#define A1_PORT PORTD
#define A1_PIN 4U

#define A2_DDR DDRC
#define A2_PORT PORTC
#define A2_PIN 0U

#define A3_DDR DDRC
#define A3_PORT PORTC
#define A3_PIN 1U

#define A4_DDR DDRC
#define A4_PORT PORTC
#define A4_PIN 2U

#define A5_DDR DDRC
#define A5_PORT PORTC
#define A5_PIN 3U

#define A6_DDR DDRC
#define A6_PORT PORTC
#define A6_PIN 4U

#define A7_DDR DDRC
#define A7_PORT PORTC
#define A7_PIN 5U

#endif
