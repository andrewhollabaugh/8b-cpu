#ifndef MCP23S17_H
#define MCP23S17_H

#include <stdint.h>

#define IOEXP_WRITE_BYTE 0x40
#define IOEXP_READ_BYTE 0x41
#define IOEXP_SET_ALL_INPUT() ioexp_write_ddr(0xff)
#define IOEXP_SET_ALL_OUTPUT() ioexp_write_ddr(0x00)

/*
 * IOCON Register Configuration
 *
 * BANK = 0
 * MIRROR = 0
 * SEQOP = 0
 * DISSLW = 0
 * HEAN = 0
 * ODR = 0
 * INTPOL = 0
 */
#define IOCON_VAL 0x00

enum register_addr {
	IODIRA_ADDR		= 0x00,
	IODIRB_ADDR		= 0x01,
	IPOLA_ADDR		= 0x02,
	IPOLB_ADDR		= 0x03,
	GPINTENA_ADDR	= 0x04,
	GPINTENB_ADDR	= 0x05,
	DEFVALA_ADDR	= 0x06,
	DEFVALB_ADDR	= 0x07,
	INTCONA_ADDR	= 0x08,
	INTCONB_ADDR	= 0x09,
	IOCON_ADDR		= 0x0a,
	IOCON2_ADDR		= 0x0b,	//Accesses the same register as IOCON
	GPPUA_ADDR		= 0x0c,
	GPPUB_ADDR		= 0x0d,
	INTFA_ADDR		= 0x0e,
	INTFB_ADDR		= 0x0f,
	INTCAPA_ADDR	= 0x10,
	INTCAPB_ADDR	= 0x11,
	GPIOA_ADDR		= 0x12,
	GPIOB_ADDR		= 0x13,
	OLATA_ADDR		= 0x14,
	OLATB_ADDR		= 0x15
};

void ioexp_init();
void ioexp_write_gpio(uint16_t pin_states);
uint16_t ioexp_read_gpio(void);
void ioexp_write_ddr(uint16_t ddr_states);
uint16_t ioexp_read_ddr(void);
void ioexp_write_pullups(uint16_t pullup_states);
uint16_t ioexp_read_pullups(void);
void ioexp_write_reg_single(uint8_t addr, uint8_t tx_data);
void ioexp_write_reg_double(uint8_t addr, uint16_t tx_data);
uint8_t ioexp_read_reg_single(uint8_t addr);
uint16_t ioexp_read_reg_double(uint8_t addr);

#endif
