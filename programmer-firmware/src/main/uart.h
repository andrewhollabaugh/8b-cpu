#ifndef UART_H
#define UART_H

#include <avr/interrupt.h>
#include <stdio.h>
#include <string.h>

#define BAUD 9600
#include <util/setbaud.h>

void uart_init();
void uart_tx(uint8_t byte);
void uart_tx_putchar(char c, FILE *stream);

#endif
