#include "uart.h"
#include "queue.h"

void uart_init()
{
    //set baud; _VALUEs are from util/setbaud.h
    UBRR0H = (uint8_t)(UBRRH_VALUE);
    UBRR0L = (uint8_t)(UBRRL_VALUE);

    UCSR0A &= ~(1U << U2X0);

    //enable tx and rx
    UCSR0B |= (1U << RXEN0);
    UCSR0B |= (1U << TXEN0);

    //enable receiver interrupt
    UCSR0B |= (1U << RXCIE0);

    //set frame: 8 bits, 1 stop bit
    UCSR0B &= ~(1U << UCSZ02);
    UCSR0C |= (1U << UCSZ01);
    UCSR0C |= (1U << UCSZ00);
}

void uart_tx(uint8_t byte)
{
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = byte;
}

/*
 * Pushes characters to the serial buffer for transmission.
 * After pushing a char, it waits for the char to leave the buffer (reg UDR0) by reading the UCSR0A flag before
 * sending the next one.
*/
void uart_tx_putchar(char c, FILE *stream)
{
    if(c == '\n')
        uart_tx_putchar('\r', stream);

    uart_tx(c);
}

/*
 * Serial receive interrupt. Pushes chars to queue if not full
 */
ISR(USART_RX_vect)
{
    cli();
    if(!queue_is_full())
        queue_push(UDR0);
    sei();
}
