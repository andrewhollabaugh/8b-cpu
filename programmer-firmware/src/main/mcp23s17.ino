
#include "mcp23s17.h"
#include "spi.h"
#include "pinout.h"

void ioexp_init()
{
	spi_init();

	//toggle NSS before starting communication, as specified in the datasheet
	SET_PIN_HIGH(SS_PORT, SS_PIN);
	SET_PIN_LOW(SS_PORT, SS_PIN);
	SET_PIN_HIGH(SS_PORT, SS_PIN);

	ioexp_write_reg_single(IOCON_ADDR, IOCON_VAL);
}

void ioexp_write_gpio(uint16_t pin_states)
{
	ioexp_write_reg_double(GPIOA_ADDR, pin_states);
}

uint16_t ioexp_read_gpio()
{
	return ioexp_read_reg_double(GPIOA_ADDR);
}

void ioexp_write_ddr(uint16_t ddr_states)
{
	ioexp_write_reg_double(IODIRA_ADDR, ddr_states);
}

uint16_t ioexp_read_ddr()
{
	return ioexp_read_reg_double(IODIRA_ADDR);
}

void ioexp_write_pullups(uint16_t pullup_states)
{
	ioexp_write_reg_double(GPPUA_ADDR, pullup_states);
}

uint16_t ioexp_read_pullups()
{
	return ioexp_read_reg_double(GPPUA_ADDR);
}

void ioexp_write_reg_single(uint8_t addr, uint8_t tx_data)
{
	SET_PIN_LOW(SS_PORT, SS_PIN);
	spi_mstr_tx_rx(IOEXP_WRITE_BYTE);
	spi_mstr_tx_rx(addr);
	spi_mstr_tx_rx(tx_data);
	SET_PIN_HIGH(SS_PORT, SS_PIN);
}

void ioexp_write_reg_double(uint8_t addr, uint16_t tx_data)
{
	uint8_t tx_data_low_byte = (uint8_t)(tx_data & 0x00ffU);
	uint8_t tx_data_high_byte = (uint8_t)((tx_data & 0xff00U) >> 8U);

	SET_PIN_LOW(SS_PORT, SS_PIN);
	spi_mstr_tx_rx(IOEXP_WRITE_BYTE);
	spi_mstr_tx_rx(addr);
	spi_mstr_tx_rx(tx_data_low_byte);
	spi_mstr_tx_rx(tx_data_high_byte);
	SET_PIN_HIGH(SS_PORT, SS_PIN);
}

uint8_t ioexp_read_reg_single(uint8_t addr)
{
	SET_PIN_LOW(SS_PORT, SS_PIN);
	spi_mstr_tx_rx(IOEXP_READ_BYTE);
	spi_mstr_tx_rx(addr);
	uint8_t rx_data = spi_mstr_tx_rx(0x00);
	SET_PIN_HIGH(SS_PORT, SS_PIN);
	return rx_data;
}

uint16_t ioexp_read_reg_double(uint8_t addr)
{
	uint8_t rx_data_low_byte, rx_data_high_byte;

	SET_PIN_LOW(SS_PORT, SS_PIN);
	spi_mstr_tx_rx(IOEXP_READ_BYTE);
	spi_mstr_tx_rx(addr);
	rx_data_low_byte = spi_mstr_tx_rx(0x00);
	rx_data_high_byte = spi_mstr_tx_rx(0x00);
	SET_PIN_HIGH(SS_PORT, SS_PIN);

	uint16_t rx_data = (rx_data_high_byte << 8U) | rx_data_low_byte;
	return rx_data;
}
